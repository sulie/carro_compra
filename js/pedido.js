var showCarrito = document.getElementById('showCarrito'); // Tabla para mostrar el resumen
window.onload = function() {
    verResumen(); // Llamada para actualizar
}

// Rellena el resumen, actua de la misma forma que verCarrito() de index.js pero de forma mas sencilla y con menos pasos
function verResumen() {
    if (typeof localStorage != undefined) {
        let content = [];

        for (let i = 0; i < localStorage.length; i++) {
            content.push(JSON.parse(localStorage.getItem(localStorage.key(i))));

            showCarrito.innerHTML +=
                "<tr>" +
                "<td>" + content[i].nombre + "</td>" +
                "<td>" + content[i].cantidad + "</td>" +
                "<td>" + content[i].precio + "€</td>" +
                "<td>" + (content[i].precio * content[i].cantidad) + "€</td>" +
                "</tr>";
        }
    }
    else {
        alert('LocalStorage error');
    }
}