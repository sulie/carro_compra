/* OBJETOS DE LOS PRODUCTOS */
const meilong = {
    id: 1,
    nombre: "Meilong",
    precio: 5,
    cantidad: 1,
    foto: "images/meilong.png"
}

const qiyi = {
    id: 2,
    nombre: "Qiyi",
    precio: 10,
    cantidad: 1,
    foto: "images/qiyi.jpg"
}

const megamix = {
    id: 3,
    nombre: "Megamix",
    precio: 15,
    cantidad: 1,
    foto: "images/megamix.jpg"
}

const gans = {
    id: 4,
    nombre: "Gans",
    precio: 60,
    cantidad: 1,
    foto: "images/gans.jpg"
}
/* FIN DE OBJETOS DE LOS PRODUCTOS */

/* BOTONES */
var botonCarrito = document.getElementById('boton-carrito'); // Rellena la tabla HTML con los datos del localStorage

// Botones de compra
var meilongBoton = document.getElementById('meilong');
var qiyiBoton = document.getElementById('qiyi');
var megamixBoton = document.getElementById('megamix');
var gansBoton = document.getElementById('gans');

/* FIN DE BOTONES */

/* EVENTOS */
botonCarrito.addEventListener("click", function() { verCarrito() });

meilongBoton.addEventListener("click", function() { addProducto(meilong) });
qiyiBoton.addEventListener("click", function() { addProducto(qiyi) });
megamixBoton.addEventListener("click", function() { addProducto(megamix) });
gansBoton.addEventListener("click", function() { addProducto(gans) });
/* FIN EVENTOS */

/* DIVS PARA RELLENARLOS CON DATOS Y BOTONES */
var showCarrito = document.getElementById('showCarrito');
var botonesCarrito = document.getElementById('botonesCarrito');
/* DIVS PARA RELLENARLOS CON DATOS Y BOTONES */

/* FUNCIONES CRUD LOCAL STORAGE */

// Añadir producto al carrito
function addProducto(producto) {
    if(typeof localStorage != undefined) {
        // Si existe el producto que queremos insertar en el localStorage, modificamos la cantidad antes de insertar
        if(localStorage.getItem(producto.id)) {
            productoLocal = JSON.parse(localStorage.getItem(producto.id));
            productoLocal.cantidad++;
            productoString = JSON.stringify(productoLocal);
            localStorage.setItem(producto.id,productoString);
        }
        // Si no existe, se inserta tal cual el objeto
        else {
            productoString = JSON.stringify(producto);
            localStorage.setItem(producto.id, productoString);
        }
        // Llamada a verCarrito para actualizar la tabla
        verCarrito();
    }
    else {
        alert('LocalStorage error');
    }
}

// Mostrar el carrito
function verCarrito() {
    if(typeof localStorage != undefined) {
        let content = []; // Array para manejar de forma mas comoda el contenido de localStorage
        // Si no hay productos en el localStorage, el carrito no muestra la tabla, si no un mensaje
        if (localStorage.length == 0) {
            showCarrito.innerHTML = "<p class='lead'>No hay productos en el carrito</p>";
        }
        // Si hay productos, se muestra la tabla y las cabeceras de la misma
        else {
            showCarrito.innerHTML = 
            "<tr>"+
                "<th>Foto</th>"+
                "<th>Producto</th>"+
                "<th>Cantidad</th>"+
                "<th>Precio</th>"+
                "<th>Eliminar</th>"+
            "</tr>";

            // Creamos un registro en la tabla para cada elemento del carrito
            for (let i = 0; i < localStorage.length; i++) {
                // Tomamos el item de la clave de cada iteracion, lo convertimos a JSON y lo almacenamos en content[] para ahorrar tiempo y facilitar el manejo de los items
                content.push(JSON.parse(localStorage.getItem(localStorage.key(i)))); 
                
                // Rellenar la tabla con tuplas
                showCarrito.innerHTML +=
                "<tr>"+
                    "<td><img width='32px' src='"+content[i].foto+"'></td>"+
                    "<td>"+content[i].nombre+"</td>"+
                    "<td>"+content[i].cantidad+"</td>"+
                    "<td>"+(content[i].precio*content[i].cantidad)+"€</td>"+
                    "<td><button type='button' class='btn btn-danger' onclick='removeProducto("+content[i].id+")'><i class='fas fa-times'></i></button></td>"+
                "</tr>";
            }

            // Botones de Enviar pedido y Vaciar carrito (solo si hay cosas en el carrito)
            botonesCarrito.innerHTML =
            "<div class='row'>"+
                "<div class='col'>"+
                    "<a class='btn btn-primary' href='pedido.html' role='button'>Enviar</a>"+
                "</div>"+
                "<div class='col'>"+
                    "<button type='button' class='btn btn-danger' onclick='vaciarCarrito()'>Vaciar</button>"+
                "</div>"+
            "</div>";
        }
    }
    else {
        alert('LocalStorage error');
    }
}

// Eliminar producto
function removeProducto(productoId) {
    // Si el producto existe y tiene mas de una unidad en el carrito, se resta en una unidad la cantidad en el carrito
    if (localStorage.getItem(productoId) && JSON.parse(localStorage.getItem(productoId)).cantidad > 1) {
        productoLocal = JSON.parse(localStorage.getItem(productoId));
        productoLocal.cantidad--;
        productoString = JSON.stringify(productoLocal);
        localStorage.setItem(productoId,productoString);
    }
    // Si el producto existe pero solo tiene una unidad en el carrito, se elimina el item directamente del carrito
    else if (localStorage.getItem(productoId)) {
        localStorage.removeItem(productoId);
    }
    // Llamada para actualizar
    verCarrito();
}

// Vaciar todo el carro
function vaciarCarrito() {
    localStorage.clear(); // Vacia el localStorage
    botonesCarrito.innerHTML = ''; // Elimina los botones del div que muestra el carrito, ya que esta vacio
    verCarrito(); // Llamada para actualizar
}